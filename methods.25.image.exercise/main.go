/*
图像
image 包定义了 Image 接口：

package image

type Image interface {
    ColorModel() color.Model
    Bounds() Rectangle
    At(x, y int) color.Color
}
注意: Bounds 方法的返回值 Rectangle 实际上是一个 image.Rectangle，它在 image 包中声明。

（请参阅文档了解全部信息。）

color.Color 和 color.Model 类型也是接口，但是通常因为直接使用预定义的实现 image.RGBA 和 image.RGBAModel 而被忽视了。这些接口和类型由 image/color 包定义。

https://tour.go-zh.org/methods/24
*/

package main

import (
	"image"
	"image/color"

	"golang.org/x/tour/pic"
)

type Image struct {
	bounds image.Rectangle
}

func (img Image) Bounds() image.Rectangle {
	return img.bounds
}

func (img Image) At(x int, y int) color.Color {
	return color.RGBA{(uint8(x+y) / 2), uint8((x + y) / 2), uint8(255), uint8(255)}
}

func (img Image) ColorModel() color.Model {
	return color.RGBAModel
}

func main() {
	m := Image{image.Rect(0, 0, 100, 100)}
	pic.ShowImage(m)
}
