/*
练习：等价二叉查找树
1. 实现 Walk 函数。

2. 测试 Walk 函数。

函数 tree.New(k) 用于构造一个随机结构的已排序二叉查找树，它保存了值 k, 2k, 3k, ..., 10k。

创建一个新的信道 ch 并且对其进行步进：

go Walk(tree.New(1), ch)
然后从信道中读取并打印 10 个值。应当是数字 1, 2, 3, ..., 10。

3. 用 Walk 实现 Same 函数来检测 t1 和 t2 是否存储了相同的值。

4. 测试 Same 函数。

Same(tree.New(1), tree.New(1)) 应当返回 true，而 Same(tree.New(1), tree.New(2)) 应当返回 false。

Tree 的文档可在这里找到。
https://tour.go-zh.org/concurrency/8
*/
package main

import (
	"fmt"

	"golang.org/x/tour/tree"
)

// Walk 步进 tree t 将所有的值从 tree 发送到 channel ch。
func Walk(t *tree.Tree, ch chan int) {
	defer close(ch)
	DFS(t, ch)
}

/*
二叉树的中序遍历
*/
func DFS(t *tree.Tree, ch chan int) {
	if t == nil {
		return
	}

	DFS(t.Left, ch)
	ch <- t.Value
	DFS(t.Right, ch)
}

// Same 检测树 t1 和 t2 是否含有相同的值。
func Same(t1, t2 *tree.Tree) bool {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go Walk(t1, ch1)
	go Walk(t2, ch2)

	for v1 := range ch1 {
		v2, ok := <-ch2
		if !ok {
			return false
		}
		if v1 != v2 {
			return false
		}
	}

	select {
	case <-ch2:
		fmt.Println("T2 has more nodes than T1.")
		return false
	default:
		return true
	}
}

func main() {
	if same := Same(tree.New(1), tree.New(1)); same {
		fmt.Println("Two trees are same.")
	} else {
		fmt.Println("Two trees are different.")
	}

	if same := Same(tree.New(1), tree.New(2)); same {
		fmt.Println("Two trees are same.")
	} else {
		fmt.Println("Two trees are different.")
	}

	if same := Same(tree.New(3), tree.New(1)); same {
		fmt.Println("Two trees are same.")
	} else {
		fmt.Println("Two trees are different.")
	}

}

/*
一、参考实现：
1、go并发之路(六)——实例：比较二叉查找树是否等价 ！！！
https://juejin.cn/post/7021532671733923848

2、Golang channel example with equivalent binary tree
https://golangexample.com/golang-channel-example-with-equivalent-binary-tree/

3、Equivalent Binary Trees Exercise, achieving the "concurrency"
https://stackoverflow.com/questions/64468668/equivalent-binary-trees-exercise-achieving-the-concurrency

4、golang_channel_example_with_equivalent_binary_tree
https://github.com/yuanyu90221/golang_channel_example_with_equivalent_binary_tree


二、知识点：
1、DFS&BFS （深度优先查询 & 广度优先查询）
https://zhuanlan.zhihu.com/p/24986203

2、二叉树的遍历方式（前序、中序、后续）
https://zhuanlan.zhihu.com/p/56895993

*/
