/*
https://tour.go-zh.org/moretypes/26
练习：斐波纳契闭包
让我们用函数做些好玩的事情。

实现一个 fibonacci 函数，它返回一个函数（闭包），该闭包返回一个斐波纳契数列 `(0, 1, 1, 2, 3, 5, ...)`。
*/
package main

import (
	"fmt"
)

// 返回一个“返回int的函数”
func fibonacci() func() int {
	var cumputeCnd int = 0
	var fibN int = 0
	var prev2 int = 0
	var prev1 int = 1

	retFunc := func() int {
		switch cumputeCnd {
		case 0:
			cumputeCnd++
			return 0
		case 1:
			cumputeCnd++
			return 1
		default:
			cumputeCnd++
			fibN = prev2 + prev1
			prev2 = prev1
			prev1 = fibN
			return fibN
		}
	}
	return retFunc
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
