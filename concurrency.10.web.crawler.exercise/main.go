package main

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

type Fetcher interface {
	// Fetch 返回 URL 的 body 内容，并且将在这个页面上找到的 URL 放到一个 slice 中。
	Fetch(url string) (body string, urls []string, err error)
}

var dict map[string]*fakeResult
var mutex sync.Mutex

// Crawl 使用 fetcher 从某个 URL 开始递归的爬取页面，直到达到最大深度。
func Crawl(url string, depth int, fetcher Fetcher, chUrls chan fakeResponse) {
	// TODO: 并行的抓取 URL
	// TODO: 不重复抓取页面。
	// 下面并没有实现上面两种情况：
	if depth <= 0 {
		return
	}

	mutex.Lock()
	if _, existed := dict[url]; existed {
		mutex.Unlock()
		return
	}
	mutex.Unlock()

	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("found: %s %q\n", url, body)
	result := fakeResult{body: body, urls: urls}
	chUrls <- fakeResponse{fakeResult: result, requestUrl: url}
	for _, u := range urls {
		go Crawl(u, depth-1, fetcher, chUrls)
	}
}

func main() {
	mutex = sync.Mutex{}
	dict = make(map[string]*fakeResult)
	chUrls := make(chan fakeResponse)
	go Crawl("https://golang.org/", 4, fetcher, chUrls)

	for {
		stop := false
		select {
		case resp := <-chUrls:

			mutex.Lock()
			dict[resp.requestUrl] = &resp.fakeResult
			mutex.Unlock()
		case <-time.After(time.Second * 3):
			fmt.Println("Timeout is detected!")
			stop = true
		}

		if stop {
			break
		}
	}

	cnt := 0
	for url, respItem := range dict {
		fmt.Println("Requesting:" + url)
		fmt.Println("Response:\r\n" + respItem.String())
		cnt++
	}
	fmt.Printf("Found %d urls in all.", cnt)

	time.Sleep(time.Duration(2) * time.Second)
}

// fakeFetcher 是返回若干结果的 Fetcher。
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (fr *fakeResult) String() string {
	sb := strings.Builder{}
	sb.WriteString("body:")
	sb.WriteString(fr.body)
	sb.WriteString("\r\n")

	sb.WriteString("urls:")
	for i := 0; i < len(fr.urls); i++ {
		sb.WriteString(fr.urls[i])
		if i != len(fr.urls)-1 {
			sb.WriteString(",")
		}
	}

	return sb.String()
}

type fakeResponse struct {
	fakeResult
	requestUrl string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher 是填充后的 fakeFetcher。
var fetcher = fakeFetcher{
	"https://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"https://golang.org/pkg/",
			"https://golang.org/cmd/",
		},
	},
	"https://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"https://golang.org/",
			"https://golang.org/cmd/",
			"https://golang.org/pkg/fmt/",
			"https://golang.org/pkg/os/",
		},
	},
	"https://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
	"https://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
}
