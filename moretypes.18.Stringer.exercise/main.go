/*
练习：Stringer
通过让 IPAddr 类型实现 fmt.Stringer 来打印点号分隔的地址。

例如，IPAddr{1, 2, 3, 4} 应当打印为 "1.2.3.4"。
https://tour.go-zh.org/methods/18
*/
package main

import (
	"fmt"
	"strconv"
	"strings"
)

type IPAddr [4]byte

// 给 IPAddr 添加一个 "String() string" 方法

func (ipa *IPAddr) String() string {
	var builder strings.Builder
	for idx, seg := range ipa {
		builder.WriteString(strconv.Itoa(int(seg)))
		if idx != 3 {
			builder.WriteString(".")
		}
	}
	return builder.String()
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		pip := &ip
		fmt.Printf("%v: %v\n", name, pip)
	}
}
