/*

练习：Reader
实现一个 Reader 类型，它产生一个 ASCII 字符 'A' 的无限流。
https://tour.go-zh.org/methods/22

reader.Validate的方法定义参考golang官网
golang.org/x/tour/reader

运行前安装相关package
go get golang.org/x/tour/reader

*/
package main

import "golang.org/x/tour/reader"

type MyReader struct{}

// TODO: 给 MyReader 添加一个 Read([]byte) (int, error) 方法
func (r MyReader) Read(slicer []byte) (int, error) {
	for i := 0; i < len(slicer); i++ {
		slicer[i] = 'A'
	}
	return len(slicer), nil
}

func main() {
	reader.Validate(MyReader{})
}
